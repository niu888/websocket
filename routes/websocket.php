<?php


use Illuminate\Http\Request;
use SwooleTW\Http\Websocket\Facades\Websocket;

/*
|--------------------------------------------------------------------------
| Websocket Routes
|--------------------------------------------------------------------------
|
| Here is where you can register websocket events for your application.
|
*/

Websocket::on('connect', function ($websocket, Request $request) {
    echo "connect";
    // called while socket on connect
});

Websocket::on('disconnect', function ($websocket) {
    // called while socket on disconnect
    echo "disconnect";
});

Websocket::on('example', function ($websocket, $data) {
    $websocket->emit('message', $data);
});

/**
 * 定义一个login路由，指向控制器方法，和http类似
 */
Websocket::on('login','App\Http\Controllers\Index\LoginController@index');
